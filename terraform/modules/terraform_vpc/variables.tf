variable "aws_region" {
  description = "AWS region"
  default     = "us-west-2"
}

variable "project_name" {
  description = "ACIT-4640 Assignment 2"
}

variable "vpc_cidr" {
  description = "VPC CIDR"
  default     = "10.0.0.0/16"
}

variable "subnet_cidr_1" {
  description = "Subnet CIDR"
  default     = "10.0.1.0/24"
}

variable "subnet_cidr_2" {
  description = "Subnet CIDR"
  default     = "10.0.2.0/24"
}

variable "subnet_name" {
  description = "Subnet Name"
  default     = "Public"
}

variable "default_route"{
  description = "Default route"
  default     = "0.0.0.0/0"
}

variable "ingress_cidr" {
  description = "Home network"
  default     = "0.0.0.0/0"
}

variable "vpc_name" {
  description = "VPC Name"
  default     = "ACIT-4640"
}
