output "vpc_id" {
  description = "The ID of the VPC"
  value       = aws_vpc.vpc_1.id
}

output "subnet_1_id" {
  description = "The ID of the first subnet"
  value       = aws_subnet.subnet_1.id
}

output "subnet_2_id" {
  description = "The ID of the second subnet"
  value       = aws_subnet.subnet_2.id
}

output "internet_gateway_id" {
  description = "The ID of the internet gateway"
  value       = aws_internet_gateway.igw_1.id
}

output "route_table_id" {
  description = "The ID of the route table"
  value       = aws_route_table.route_table_1.id
}

output "security_group_id" {
  description = "The ID of the security group"
  value       = aws_security_group.security_group.id
}
