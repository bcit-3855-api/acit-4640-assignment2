output "ssh_pub_key_file" {
  value = module.ssh_key.pub_key_file
}

output "ssh_priv_key_file" {
  value = module.ssh_key.priv_key_file
}

output "Ec2_Public_IP_1" {
  value = module.ec2.EC2_Public_1
}

output "EC2_Public_IP_2" {
  value = module.ec2.EC2_Public_2
}

output "instance1" {
  value = module.ec2.instance_ids_1
}

output "instance2" {
  value = module.ec2.instance_id_2
}

output "subnet1" {
  value = module.vpc.subnet_1_id
}
output "subnet2" {
  value = module.vpc.subnet_2_id
}




