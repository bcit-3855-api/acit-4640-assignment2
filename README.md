# 🌐 Network Provisioning - Assignment 2 🌐

Welcome! This repository houses the code and configuration files for the Network Provisioning Assignment 2.

## 🎥 Video Demo

A video demo of the project can be found [here](https://www.youtube.com/watch?v=8dZnbIqyNXQ).

## 📋 Prerequisites

Ensure the following are installed before running the configuration:

- Terraform
- Ansible
- 4640 Starter Code/Repo
- AWS CLI

## 🛠️ Installation 

### Terraform
Follow this [link](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli) to install Terraform.

### Ansible
Install Ansible using this [link](https://docs.ansible.com/ansible/latest/installation_guide/index.html).

### 4640 Starter Code/Repo
Clone the 4640 Starter Code/Repo from this [link](https://gitlab.com/cit_4640/4640-assignment-app-files).

### AWS CLI
Install AWS CLI using this [link](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html).

## ⚙️ Configuration 

### Terraform Configuration
Navigate to the `acit-4640-assignment2/terraform` directory and run the following commands:

1. `terraform init` - Initializes the working directory containing Terraform configuration files.
2. `terraform plan` - Creates an execution plan.
3. `terraform apply` - Applies the changes required to reach the desired state of the configuration.

Running `terraform apply` will automatically create the following resources:

- VPC
- Subnets
- Route Tables
- Internet Gateway
- Security Groups
- EC2 Instances
- SSH Key Pair

It will also output the plan to the console at the end of the run and create 2 files inside the `acit-4640-assignment2/ansible` folder:

- `inventory.yml` - Contains the IP addresses of the EC2 instances that were created along with the SSH key path.
- `ansible.cfg` - Contains the configuration for Ansible to use the `inventory.yml` file.

### Ansible Configuration
Navigate to the `acit-4640-assignment2/ansible` directory and run the following command:

- `ansible-playbook playbook.yml` - Runs the playbook and configures the EC2 instances with the required packages and configurations.

After running the playbook, it will run through the tasks and output the result of each task to the console.

## 🚀 Running the Application

After running the Ansible playbook, the application will be live on the EC2 instances. Access the application by navigating to the public IP address of the EC2 instance in your web browser. You should see a todo list application. The data should persist in both instances even after refreshing the page.

## 📜 Commands Used

Here's a list of commands used to complete the assignment:

- `terraform init`
- `terraform plan`
- `terraform apply`
- `terraform destroy`
- `ansible-playbook playbook.yml`